$(function(){
    var numItems = $('.message-item').length;
    var sound = document.getElementById('notification');
    var chBox = $('div.message');
    var chWrap = $('div.message-wrapper');
    var interval;
    $(window).on('focus', function(){
        $(window).trigger('focus.mess')
    })

    $('#messInput').focus();
    sound.volume-=0.5;
    checkHeight(chBox, chWrap);
    $(document).on('click', '#show', function(){
        numItems = $('.message-item').length;
        getItems(numItems);
    });
    setInterval(function(){
        numItems = $('.message-item').length;
        getItems(numItems);
    }, 500);
    setInterval(function(){
        checkUsers();
    }, 30000);

    function getItems (num){
        $.get("/chat/ajax.message.php", {'param': num}, function(result){
            if(result) {
                var data = JSON.parse(result);
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var elem = '<div class="message-item" data-name="'+escapeHtml(data[i].name)+'" data-time="'+data[i].time+'"><span class="time">' + escapeHtml(date("H:i:s", data[i].time)) + '</span><span class="name">' + escapeHtml(data[i].name) + '</span><span class="message">' + escapeHtml(data[i].message) + '</span></div>';
                    $('div.message').append(elem);
                }
                sound.play();
                checkHeight(chBox, chWrap);
                messAnounce()
            }
        })
    }
    function checkUsers (){
        $('#online').children('div').each(function(){
            var name = $(this).find('span').text();
            var time = parseInt($('.message-item[data-name="' + name + '"]').last().attr('data-time')),
                n = new Date(),
                now = n.getTime() / 1000;

            if(time + 60 < now) {
                window.location.href = "/chat/users.php?name=" + name;
            }
        })
    }
    function checkHeight (el1, el2){
        var h1 = el1.height(),
            h2 = el2.outerHeight();

        if(h1 <= h2) {
            el1.scrollTop(h2);
            console.log('worked')
        }
    }
    function escapeHtml(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) { return map[m]; });
    }
    function messAnounce(){
        var arr = ['Man...', 'U got message!'],
            count = 0;

        $('#favicon').attr('href', 'fav.png');
        interval = setInterval(function(){
            count = count == 0 ? 1 : 0;
            $('title').text(arr[count]);
        }, 700);

        $(window).bind('focus.mess, mouseover.mess', function(){
            clearInterval(interval);
            $('#favicon').attr('href', 'fav2.jpg');
            $('title').text('Типа чат');
            $(window).unbind('focus.mess, mouseover.mess')
        })
    }
});
