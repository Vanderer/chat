<?php
$param = $_GET['param'];

    $DBH = new PDO("sqlite:database.db");
    $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBH->exec("CREATE TABLE IF NOT EXISTS messages (name TEXT, message TEXT, time STRING)");


    $st = $DBH->query('SELECT * FROM messages');
    $result = $st->fetchAll();
    $num = count($result);
    if($num > $param) {
        $response = [];
        for ($i = $param; $i < $num; $i++) {
            $j['name'] = $result[$i]['name'];
            $j['message'] = $result[$i]['message'];
            $j['time'] = $result[$i]['time'];
            array_push($response, $j);
        }
        $response = json_encode($response);
        echo $response;
    } else {
        return false;
    }
?>


