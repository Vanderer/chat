<?
    session_start();
    $user = '';
    if ($_SESSION['username']) {
        $user = $_SESSION['username'];
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Типа чат</title>

    <link rel="stylesheet" href="css/default.css"/>
    <link id="favicon" rel="icon" href="fav2.jpg"/>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/date.js"></script>
    <script src="js/default.js"></script>

</head>
<body>
    <div class="wrapper">
        <? if (!$user) { ?>
        <div class="authorisation" >
            <h2>Ентер йор нейм</h2>
            <form action="reg.php" method="post">
                <input name="name" type="text"/>
                <input type="submit"/>
            </form>
        </div>
        <? } else{ ?>
        <h1><?= "Hello ".$_SESSION['username']?></h1>
        <div class="chat">
            <div class="message">
                <div class="message-wrapper">
                    <?
                    $DBH = new PDO("sqlite:database.db");
                    $DBH->exec("CREATE TABLE IF NOT EXISTS messages (name TEXT, message TEXT, time INTEGER)");
                    $st = $DBH->query('SELECT * FROM messages');
                    $result = $st->fetchAll();
                    foreach ($result as $row) {
                        echo '<div class="message-item" data-time="'.$row['time'].'" data-name="'.$row['name'].'">';
                        echo '<span class="time">'.htmlspecialchars(date("H:i:s", $row['time'])).'</span>';
                        echo '<span class="name">'.htmlspecialchars($row['name']).'</span>';
                        echo '<span class="message">'.htmlspecialchars($row['message']).'</span>';
                        echo '</div>';
                    }
                    //$DBH = null;
                    ?>
                </div>
            </div>
            <div class="user-box" id="online">
                <?
                $st = $DBH->query('SELECT * FROM users');
                $result = $st->fetchAll();
                foreach ($result as $row) {
                    echo '<div class="user-item">';
                    echo '<span>'.htmlspecialchars($row['name']).'</span>';
                    echo '</div>';
                }
                $DBH = null;
                ?>
            </div>
            <div class="clear"></div>
            <form class="leave-message" action="message.php" method="get">
                <input id="messInput" type="text" name="message">
                <input class="submit-message" type="submit" value="S"/>
            </form>
<!--            <div id="show">SHOW</div>-->
        </div>
            <a class="exit" href="/chat/exit.php">ВЫХОД</a>
        <audio id="notification" src="viber_original_sms.mp3"></audio>
        <? } ?>
    </div>
</body>
</html>