<?php
    $name = $_POST['name'];
    session_start();
    $_SESSION['username'] = $name;

try{
    $DBH = new PDO("sqlite:database.db");
    $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBH->exec("CREATE TABLE IF NOT EXISTS users (name TEXT, time STRING)");

    $insert = 'INSERT INTO users (name, time) VALUES (:name, :time)';
    $stmt = $DBH->prepare($insert);
    $stmt->bindParam(':name', $n);
    $stmt->bindParam(':time', $t);

    $n = $name;
    $t = time();
    $stmt->execute();

    $DBH = null;
    //$DBH->exec('DROP TABLE messages');
} catch (PDOException $e) {
    die($e->getMessage());
}
header("Location: http://".$_SERVER['HTTP_HOST']."/chat/index.php");