<?php

$name = $_GET['name'];

try{
    $DBH = new PDO("sqlite:database.db");
    $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBH->exec("CREATE TABLE IF NOT EXISTS users (name TEXT, time STRING)");

    $st = $DBH->query('SELECT * FROM users');
    $result = $st->fetchAll();

    $DBH->exec('DROP TABLE users');
    $DBH->exec("CREATE TABLE IF NOT EXISTS users (name TEXT, time STRING)");

    $insert = 'INSERT INTO users (name, time) VALUES (:name, :time)';
    $stmt = $DBH->prepare($insert);
    foreach ($result as $row) {
        if ($row['name'] == $name) {
            unset($row);
        } else {
            $stmt->bindParam(':name', $n);
            $stmt->bindParam(':time', $t);

            $n = $row['name'];
            $t = $row['time'];
            $stmt->execute();
        }
    }


    $DBH = null;
    //$DBH->exec('DROP TABLE messages');
} catch (PDOException $e) {
    die($e->getMessage());
}
header("Location: http://".$_SERVER['HTTP_HOST']."/chat/index.php");