<?php
session_start();
$name = $_SESSION['username'];
$message = $_GET['message'];
$time = time();

try{
    $DBH = new PDO("sqlite:database.db");
    $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBH->exec("CREATE TABLE IF NOT EXISTS messages (name TEXT, message TEXT, time STRING)");

    $insert = 'INSERT INTO messages (name, message, time) VALUES (:name, :message, :time)';
    $stmt = $DBH->prepare($insert);
    $stmt->bindParam(':name', $n);
    $stmt->bindParam(':message', $m);
    $stmt->bindParam(':time', $t);

    $n = $name;
    $m = $message;
    $t = $time;
    $stmt->execute();

    //$st = $DBH->query('SELECT * FROM messages');
    //$result = $st->fetchAll();
    //$DBH->exec('DROP TABLE messages');
   // foreach ($result as $row) {
     //   echo 'id = '.$row['id'].', name = '.$row['name'].', message = '.$row['message'].', time = '.$row['time']."\n";
   // }
    $DBH->exec("CREATE TABLE IF NOT EXISTS users (name TEXT, time STRING)");
    $st = $DBH->query('SELECT * FROM users');
    $result = $st->fetchAll();
    $allGood = false;
    foreach ($result as $row) {
        if ($row['name'] == $name) {
            $allGood = true;
        }
    }
    if(!$allGood) {
        $insert = 'INSERT INTO users (name, time) VALUES (:name, :time)';
        $stmt = $DBH->prepare($insert);
        $stmt->bindParam(':name', $n);
        $stmt->bindParam(':time', $t);

        $n = $name;
        $t = time();
        $stmt->execute();
    }
    $DBH = null;
} catch (PDOException $e) {
    die($e->getMessage());
}
header("Location: http://".$_SERVER['HTTP_HOST']."/chat/index.php");
?>